<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    protected $guarded=[];
    public function itemPack()
    {
        return $this->hasMany('O2pluss\O2logis\Data\ItemPack');
    }
}
