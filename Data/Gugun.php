<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class Gugun extends Model
{
    //
    protected $guarded = [];

    public function dongs()
    {
        return $this->hasMany('O2pluss\O2logis\Data\Dong');
    }

    public function sido()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\Sido');
    }
}
