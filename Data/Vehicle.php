<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    //
    public function rider()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\Rider');
    }
}
