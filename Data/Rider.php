<?php

namespace O2pluss\O2logis\Data;

use Illuminate\Database\Eloquent\Model;

class Rider extends Model
{
    //
    public function vehicles()
    {
        return $this->hasMany('O2pluss\O2logis\Data\Vehicle');
    }

    public function partner()
    {
        return $this->belongsTo('O2pluss\O2logis\Data\Partner');
    }
}
