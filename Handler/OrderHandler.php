<?php
/**
 * Created by PhpStorm.
 * User: kimyu
 * Date: 2018-03-27
 * Time: 오후 3:42
 */

namespace O2pluss\O2logis\Handler;



use App\Obj\Order;

interface OrderHandler
{
    public function createWith($parmeter);

    public function update();

    public function delete();
}