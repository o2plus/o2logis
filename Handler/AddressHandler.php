<?php
/**
 * Created by PhpStorm.
 * User: kimyu
 * Date: 2018-03-28
 * Time: 오후 7:24
 */

namespace O2pluss\O2logis\Handler;


use O2pluss\O2logis\Data\Address;

abstract class AddressHandler
{
    /**
     * @param $keyword
     * @return Address $address;
     */
    abstract function search($keyword);

    protected function getOrSet($sido,$gugun,$dong,$road_address,$full_address,$lat,$lon){

        $sido_id=\O2pluss\O2logis\Data\Sido::firstOrCreate(['name'=>$sido])->id;
        $gugun_id=\O2pluss\O2logis\Data\Gugun::firstOrCreate(['name'=>$gugun,'sido_id'=>$sido_id])->id;
        $dong_id=\O2pluss\O2logis\Data\Dong::firstOrCreate(['name'=>$dong,'gugun_id'=>$gugun_id])->id;
        $addressData=\O2pluss\O2logis\Data\Address::firstOrCreate(
            ['full_address'=>$full_address,
            'road_address'=>$road_address,
            'dong_id'=>$dong_id,
            'lat'=>substr($lat,0,12),
            'lng'=>substr($lon,0,12)]
        );
        return $addressData;

    }
}