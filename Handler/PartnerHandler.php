<?php
/**
 * Created by PhpStorm.
 * User: kimyu
 * Date: 2018-03-28
 * Time: 오후 1:51
 */

namespace O2pluss\O2logis\Handler;


use O2pluss\O2logis\Obj\Order;
use O2pluss\O2logis\Obj\Partner;

interface PartnerHandler
{
    /**
     * @param $parameter
     * @return Partner $partner
     */
    public function addPartnerWith($parameter);

    /**
     * @param Order $order
     * @return Partner $partner
     */
    public function findPartnerBy(Order $order);

    /**
     * @param Partner $partner
     * @return mixed
     */
    public function update(Partner $partner);

}