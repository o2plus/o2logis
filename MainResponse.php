<?php
/**
 * Created by PhpStorm.
 * User: kimyu
 * Date: 2018-03-28
 * Time: 오후 1:29
 */

namespace O2pluss\O2logis;


use Illuminate\Contracts\Support\Responsable;

class MainResponse implements Responsable
{
    public $status=200;
    public $msg="예상치 못한 오류입니다.";
    public $body=[];

    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        $this->status=http_response_code();
        if($this->status==200){
            return $this->successMessage();
        }else{
            return $this->failMessage();
        }
    }

    public function successMessage()
    {
        return [
            'status'=>$this->status,
            'body'=>$this->body
        ];
    }

    public function failMessage()
    {
        return [
            'status'=>$this->status,
            'msg'=>$this->msg
        ];
    }
}