<?php
/**
 * Created by PhpStorm.
 * User: kimyu
 * Date: 2018-03-27
 * Time: 오후 3:04
 */

namespace O2pluss\O2logis\Obj;


interface Partner
{
    public function getName();

    public function setName();

    public function getCharge();

    public function setCharge($sido,$gugun = null,$dong = null);

    public function regist(Order $order);

    public function update(Order $order);

    public function cancel(Order $order);

    public function getPartnerOrder(Order $order);
}