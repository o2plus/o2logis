<?php
/**
 * Created by PhpStorm.
 * User: kimyu
 * Date: 2018-03-27
 * Time: 오후 3:07
 */

namespace O2pluss\O2logis\Obj;


class Address
{
    private $data;
    public function __construct($id)
    {
        $this->data=\O2pluss\O2logis\Data\Address::with('dong.gugun.sido')->find($id);
    }

    public function getSido()
    {
        return $this->data->dong->gugun->sido->name;
    }

    public function getGugun()
    {
        return $this->data->dong->gugun->name;
    }

    public function getDong()
    {
        return $this->data->dong->name;
    }

    public function getLatLng()
    {
        return [
            $this->data->lat,
            $this->data->lng
        ];
    }

    public function getFullAddress()
    {
        return $this->data->full_address;
    }

    public function getRoadAddress()
    {
        return $this->data->road_address;
    }
}