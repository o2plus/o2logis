<?php
/**
 * Created by PhpStorm.
 * User: kimyu
 * Date: 2018-03-28
 * Time: 오후 1:07
 */

namespace O2pluss\O2logis;


use O2pluss\O2logis\Handler\OrderHandler;
use O2pluss\O2logis\Handler\PartnerHandler;
use O2pluss\O2logis\Obj\Order;
use O2pluss\O2logis\Obj\Partner;

class OrderService
{
    private $orderHandler;
    private $partnerHandler;

    public function __construct(OrderHandler $orderHandler,PartnerHandler $partnerHandler)
    {
        $this->orderHandler=$orderHandler;
        $this->partnerHandler=$partnerHandler;
    }

    public function registerOrder(array $parm){
        $order=$this->orderHandler->createWith($parm);
        $this->processOrder($order);
    }

    public function processOrder(Order $order, Partner $partner = null){
        if(is_null($partner))$partner=$this->findPartner($order);
        $partner->regist($order);
    }

    public function cancelOrder(Order $order,Partner $partner = null)
    {
        if(is_null($partner)){
            $partnerList=$order->getPartnerList();
            $partnerList->each(function($partner)use($order){
                $partner->cancel($order);
            });
        }else{
            $partner->cancel($order);
        }
    }

    public function findPartner(Order $order)
    {
        return $this->partnerHandler->getMatchPartnerBy($order);
    }
}